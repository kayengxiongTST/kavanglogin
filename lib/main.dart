import 'package:flutter/material.dart';
import 'package:myapp_otp1/Screens/welcome/welcome_screen.dart';
import 'package:myapp_otp1/constarts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var wecomeScreen = WecomeScreen;
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Auth',
        theme: ThemeData(
          primaryColor: kPrimaryColor,
          scaffoldBackgroundColor: Colors.white,
        ),
        home: WecomeScreen());
  }
}
