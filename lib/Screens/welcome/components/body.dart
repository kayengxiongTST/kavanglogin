import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:myapp_otp1/Screens/Login/login_screen.dart';
import 'package:myapp_otp1/Screens/Signup/signup_screen.dart';
import 'package:myapp_otp1/Screens/welcome/components/background.dart';
import 'package:myapp_otp1/components/rounded_button.dart';

import '../../../constarts.dart';

class Body extends StatelessWidget {
  // const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'WE COME TO EDU',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            SvgPicture.asset(
              "assets/icons/chat.svg",
              height: size.height * 0.45,
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            RoundedButton(
              press: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              },
              text: 'LOGIN',
            ),
            RoundedButton(
                press: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignUpScreen()));
                },
                text: 'SIGNIN',
                color: kPrimaryColor,
                textColor: Colors.black),
          ],
        ),
      ),
    );
  }
}
