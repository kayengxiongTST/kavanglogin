import 'package:flutter/material.dart';
import 'package:myapp_otp1/components/text_field_container.dart';
import 'package:myapp_otp1/constarts.dart';

class RoundedPasswordField extends StatelessWidget {
 final ValueChanged<String> onChanged;
 
  const RoundedPasswordField({
    Key? key, required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
        child: TextField(
          obscureText: false,
          onChanged: onChanged,
            decoration: InputDecoration(
              hintText:'Password',
                icon: Icon(Icons.lock, color: kPrimaryColor),
                suffixIcon: Icon(Icons.visibility, color: kPrimaryColor),
                border: InputBorder.none)));
  }
}