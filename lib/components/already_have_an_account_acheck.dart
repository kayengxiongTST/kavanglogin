import 'package:flutter/material.dart';
import 'package:myapp_otp1/constarts.dart';

class AlreadyHaveAnAccountCheck extends StatelessWidget {
  final bool login;
  final Function press;

  const AlreadyHaveAnAccountCheck({
    Key? key,
    this.login = true,
    required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(login ? "Don't Have an Account ?" : " Already Have Account ?",
            style: TextStyle(color: kPrimaryColor)),
        GestureDetector(
          onTap: () {
            press();
          },
          child: Text(login ? "Sign Up" : "Sigin In",
              style:
                  TextStyle(color: kPrimaryColor, fontWeight: FontWeight.bold)),
        ),
      ],
    );
  }
}
